---
## Setting up

Notes: Use IntelliJ to import this as a Gradle project straight from the master branch clone.

1. Clone the master branch
2. Import it to IntelliJ as a Gradle Project
   (Troubleshoot, should we have any incompatible versions please make sure your settings do look like that)
   ![img.png](img.png)
3. If everything is built successfully go ahead and run everything through test/testRunners/TestRunner.java
   ![img_1.png](img_1.png)
---
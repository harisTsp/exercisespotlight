Feature: Demo

  Scenario: Example 1
    Given I am on "https://www.racingpost.com/racecards/"
    When I click "Big Race Entries"
    Then I click event 1 on table "Big Race Entries"

  Scenario: Some style points for the same steps working in other places of the application (potentially everywhere?)
    Given I am on "https://www.racingpost.com/racecards/"
    When I click "News"
    Then I click event 1 on table "Race Reports"

  Scenario: Some style points for the same steps working in other places of the application (potentially everywhere?)
    Given I am on "https://www.racingpost.com/racecards/"
    When I click "News"
    Then I click event 1 on table "Previews"
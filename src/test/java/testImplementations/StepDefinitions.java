package testImplementations;

import elementUtilities.SpotlightTableUtilities;
import exceptions.InvalidAlertSelectionParameter;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import testRunners.BeforeHooks;

public class StepDefinitions {
    final WebDriver driver = BeforeHooks.driver;
    final SpotlightTableUtilities tableUtil = new SpotlightTableUtilities();

    @Given("I am on {string}")
    public void iAmOn(final String urlString) {
        driver.get(urlString);
    }

    @When("I click {string}")
    public void iClickTheHeader(final String linkText) {
        driver.findElement(By.linkText(linkText)).click();
    }

    @Then("I click event {int} on table {string}")
    public void iClickEventOnTable(final int indexOfOption, final String tableName) {
        driver.findElements(By.tagName("main"));
        tableUtil.clickResultFromTable(driver, indexOfOption, tableName);
    }

    @And("I click {string} on alert")
    public void iClickOnAlert(final String option) throws InvalidAlertSelectionParameter {
        if (option.equalsIgnoreCase("yes")){
            driver.switchTo().alert().accept();
        }else if (option.equalsIgnoreCase("no")){
            driver.switchTo().alert().dismiss();
        }else{
            throw new InvalidAlertSelectionParameter("Please provide with a Yes or No when using the step 'I click {string} on alert' step");
        }
    }
}

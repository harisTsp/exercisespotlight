package testRunners;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

/**
 * testRunners.BeforeHooks is responsible for any setup we need for tests like setting up the driver, keeping important variables etc.
 */
public class BeforeHooks {
    public static WebDriver driver;
    public static String testServer;
    public static Actions actions;

    static void selectTestServer() {
        testServer = "https://www.racingpost.com/racecards/";
    }

    /**
     * @see WebDriverManager is a dependency that is getting the driver and is setting up an instance like a cdn.
     */
    static void createChromeDriver() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        actions = new Actions(driver);
    }
}

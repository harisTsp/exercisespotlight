package testRunners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

/**
 * testRunners.TestRunner example.
 * This class helps Cucumber run a features directory against a test implementation directory by using
 * CucumberOptions with parameters:
 * features = step implementation directory paths
 * glue = cucumber features file
 * plugin = generates report on defined path
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"C:/Users/haris/IdeaProjects/exam/src/test/resources"},
        glue = {"testImplementations"},
        plugin = {"json:target/cucumber-report.json"},
        snippets = CucumberOptions.SnippetType.CAMELCASE
)

public class TestRunner {
    @BeforeClass
    public static void setUp() {
        BeforeHooks.selectTestServer();
        BeforeHooks.createChromeDriver();
    }
}
package exceptions;

import testImplementations.StepDefinitions;

/**
 * Exception created to be thrown when someone is misusing the {@link StepDefinitions#iClickOnAlert(String)}
 */
public class InvalidAlertSelectionParameter extends Exception {
    public InvalidAlertSelectionParameter(final String errorMessage) {
        super(errorMessage);
    }
}

package elementUtilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.JavaScriptFunctionWrapper;

import java.util.List;

/**
 * Class to keep the logic we need to interact with Tables in general for Spotlight org.
 */
public class SpotlightTableUtilities {
    final String XPATH_FOR_HEADERS = "//h2 | //h1";

    public synchronized void clickResultFromTable(final WebDriver driver, final int indexOfOption, final String tableName) {
        for (final WebElement h1Element : driver.findElements(By.xpath(XPATH_FOR_HEADERS))) {
            if (h1Element.getText().equalsIgnoreCase(tableName)) {
                final WebElement parentDiv = JavaScriptFunctionWrapper.getParentElement(driver, h1Element);
                final List<WebElement> listOfOptions = parentDiv.findElement(By.tagName("div")).findElements(By.tagName("a"));
                JavaScriptFunctionWrapper.clickElement(driver,listOfOptions.get(indexOfOption-1));
                break;
            }
        }
    }
}

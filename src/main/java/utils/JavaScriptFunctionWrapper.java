package utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.File;

public class JavaScriptFunctionWrapper {
    /**
     * Gets parent element of the parametrised element. Example:
     * <div id = "parent">
     *    <h1 id = "child"/>
     * </div>
     *
     * @param child WebElement found by id "child"
     * @return WebElement with id = "parent"
     */
    public static WebElement getParentElement(final WebDriver driver, final WebElement child) {
        final String pathToFunction = new File("./src/main/java/elementUtilities/javaScriptFunctions/GetParentElement.js").getAbsolutePath();
        final String scrollScript = JavaScriptReader.readJavaScript(pathToFunction);
        return ((WebElement) ((JavascriptExecutor) driver).executeScript(scrollScript, child));
    }

    /**
     * Clicks element with id element to click:
     * <div id = "wrapper div">
     *    <h1 id = "elementToClick"/>
     * </div>
     *
     * @param elementToClick WebElement passed in by the WebDriver, this method only clicks it.
     */
    public static void clickElement(final WebDriver driver, final WebElement elementToClick) {
        final String pathToFunction = new File("./src/main/java/elementUtilities/javaScriptFunctions/ClickElement.js").getAbsolutePath();
        final String scrollScript = JavaScriptReader.readJavaScript(pathToFunction);
        ((JavascriptExecutor) driver).executeScript(scrollScript, elementToClick);
    }
}

package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class JavaScriptReader {

    /**
     * Reads JavaScript functions from {@link elementUtilities.javaScriptFunctions}, then skips the first line by doing br.readLine()
     * in the while reads the next line and checks if null.
     *
     * If the line is more than 3 characters means its not the end of the file and we need that to come back.
     *
     * @param filePath URL to file
     * @return The JavaScript we written in that file.
     */
    public static String readJavaScript(final String filePath) {
        final StringBuilder stringBuilder = new StringBuilder();
        try {
            final BufferedReader br;
            final File inputFile = new File(filePath);
            br = new BufferedReader(new FileReader(inputFile));
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                if (line.length() > 3) {
                    stringBuilder.append(line);
                }
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
